import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import {
  Button,
  PageHeader,
  Descriptions,
  Input,
  message,
  Form,
  Select,
  DatePicker,
} from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );
  const [isEditView, setEditView] = useState(false);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const onEdit = () => {
    setEditView((edit) => !edit);
  };

  const onFinish = (values: any) => {
    console.log('Success:', values);
    console.log(data);

    save({
      ...data,
      ...values,
      email: data.email,
      birthday: values.birthday.toISOString(), //save the ISO, should ask for the actual format to save it
    });
    setEditView(false);
  };

  const onFinishFailed = (errorInfo: any) => {
    message.error("The user doesn't exist redirecting back...", 2);
    console.log(errorInfo); //> save into a error dashboard
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button
            type="default"
            onClick={() => {
              onEdit();
            }}
          >
            {isEditView ? 'Go Back' : 'Edit'}
          </Button>,
        ]}
      >
        {data && !isEditView && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>
            <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
          </Descriptions>
        )}

        {!isEditView ? (
          <GenericList<Company>
            loading={loading}
            extra={ResponsiveListCard}
            data={data && data.companyHistory}
            ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
            handleLoadMore={() => {}}
            hasMore={false}
          />
        ) : null}

        {isEditView ? (
          <div id="edit-info-form">
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Name"
                name="name"
                rules={[{ required: true, message: 'Please input your name!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Gender"
                name="gender"
                rules={[{ required: true, message: 'Please input your gender!' }]}
              >
                <Select placeholder="Select a option" allowClear>
                  <Select.Option value="male">male</Select.Option>
                  <Select.Option value="female">female</Select.Option>
                  <Select.Option value="other">other</Select.Option>
                </Select>
              </Form.Item>

              <Form.Item
                label="Phone"
                name="phone"
                rules={[{ required: true, message: 'Please input your phone!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Birthday"
                name="birthday"
                rules={[{ required: true, message: 'Please input your birthday!' }]}
              >
                <DatePicker />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        ) : null}
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
